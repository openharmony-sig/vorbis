# vorbis

## 简介
> 一种通用音频和音乐编码格式。
> Vorbis编解码器规范属于公共领域。所有技术细节都已发布并记录，任何软件实体都可以充分利用该格式，而无需支付许可费、版税或专利问题。

![](screenshot/result.gif)

## 下载安装
直接在OpenHarmony-SIG仓中搜索vorbis并下载。

## 使用说明
以OpenHarmony 3.1 Beta的rk3568版本为例

1. 将下载的vorbis库代码存在以下路径：./third_party/vorbis

2. 将libogg库一起放入third_party目录

3. 修改添加依赖的编译脚本，路径：/developtools/bytrace_standard/ohos.build

```

{
  "subsystem": "developtools",
  "parts": {
    "bytrace_standard": {
      "module_list": [
        "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
        "//developtools/bytrace_standard/bin:bytrace_target",
        "//developtools/bytrace_standard/bin:bytrace.cfg",
        "//developtools/bytrace_standard/interfaces/kits/js/napi:bytrace",
        "//third_party/libogg:libogg",
        "//third_party/vorbis:vorbis_test",
        "//third_party/vorbis:vorbisfile",
        "//third_party/vorbis:vorbisenc",
        "//third_party/vorbis:vorbis"
      ],
      "inner_kits": [
        {
          "type": "so",
          "name": "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
          "header": {
            "header_files": [
              "bytrace.h"
            ],
            "header_base": "//developtools/bytrace_standard/interfaces/innerkits/native/include"
          }
        }
      ],
      "test_list": [
        "//developtools/bytrace_standard/bin/test:unittest"
      ]
    }
  }
}
   
```

4. 编译：./build.sh --product-name rk3568 --ccache

5. 生成库文件和一些可执行测试文件，路径：out/rk3568/developtools/profiler

## 接口说明
1. Vorbis headers处理：
   `vorbis_info_init()`
   `vorbis_comment_init()`
2. 转换到任何PCM格式并输出：
   `vorbis_synthesis_pcmout()`
3. 清理libvorbis存储：
   `vorbis_block_clear()`
   `vorbis_dsp_clear()`
4. 清理逻辑位流：
   `vorbis_comment_clear()`
   `vorbis_info_clear()`
5. 使用VBR质量模式：
   `vorbis_encode_init_vbr()`
6. 使用平均比特率模式：
   `vorbis_encode_init()`
7. 设置分析状态和辅助编码存储：
   `vorbis_analysis_init()`
   `vorbis_block_init()`
8. 使用比特率管理：
   `vorbis_analysis()`
   `vorbis_bitrate_addblock()`
9. vorbis进行一些数据预分析，然后划分块进行更复杂的处理：
   `vorbis_analysis_blockout()`

## 约束与限制

在下述版本验证通过：

DevEco Studio 版本：3.1 Beta1(3.1.0.200)，SDK:API9 Beta5(3.2.10.6)

## 目录结构
````
|---- vorbis
|     |---- cmake      #编译文件
|     |---- contrib    #OSS-Fuzz构建脚本
|     |---- debian     #用于构建Debian的规则/规范文件deb包
|     |---- doc        #Vorbis文件
|     |---- examples   #libvorbis、libvorbisfile和libvorbisenc的编程使用的示例代码
|     |---- include    #头文件
|           |---- vorbis
|           |---- vorbisenc.h                  #设置编码器所需的操作
|           |---- vorbisfile.h                 #基于stdio的方便库，用于打开/查找/解码
|     |---- lib        #Vorbis音频编码格式的实现
|           |---- books
|                 |---- coupled
|                 |---- res_books_51.h         #5.1环绕声的静态码本
|                       |---- res_books_stereo.h     #由huff/huffbuld自动生成的静态码本
|                 |---- floor
|                       |---- floor_books.h          #由huff/huffbuld自动生成的静态码本
|                 |---- uncoupled
|                       |---- res_books_uncoupled.h  #由huff/huffbuld自动生成的静态码本
|           |---- modes
|                 |---- floor_all.h                  #关键floor设置
|                 |---- psych_8.h                    #8kHz心理声学设置
|                 |---- setup_44p51.h                #44.1/48kHz 5.1环绕声模式的顶级设置
|           |---- analysis.c                         #单块PCM分析模式调度
|           |---- block.c                            #PCM数据向量阻塞、开窗和拆卸/重新组装
|           |---- codebook.c                         #基本码本打包/解包/编码/解码操作
|           |---- envelope.c                         #PCM数据包络分析
|           |---- mapping0.c                         #通道映射0实现
|           |---- registry.c                         #time, floor, res后端和通道映射注册表
|           |---- synthesis.c                        #单块PCM合成
|           |---- vorbisenc.c                        #编码器模式设置的简单编程接口
|           |---- vorbisfile.c                       #基于stdio的便利库，用于打开/查找/解码
|           |---- window.c                           #窗口功能
|     |---- m4         #为libogg配置路径
|     |---- macosx     #MacOS X的项目文件
|     |---- symbian    #禁用某些警告
|     |---- test       #测试代码
|     |---- vq         #辅助码本
|     |---- win32      #Win32项目文件和生成自动化
|     |---- README.md  # 安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/vorbis/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/vorbis/pulls) 。

## 开源协议
本项目基于 [BSD-3-Clause License](https://gitee.com/openharmony-sig/vorbis/blob/master/COPYING) ，请自由地享受和参与开源。